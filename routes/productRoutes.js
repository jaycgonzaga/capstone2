const express = require('express');
const router = express.Router();
const ProductController = require('../controllers/ProductController.js');
const auth = require('../auth.js');

// Add product to database
router.post('/addproduct', auth.verify, auth.verifyAdmin, (request, response) => {
	ProductController.createListing(request.body).then((result) => {
		response.send(result);
	})
})

// View all existing product
router.get('/all', (request, response) => {
	ProductController.getAllProducts(request, response);
})
module.exports = router;

// View all existing and active product
router.get('/', (request, response) =>{
	ProductController.getAllActiveProduct(request, response);
})

// Search single product using id
router.get('/:id', (request, response) => {
	ProductController.getProduct (request, response);
})

// Search and update single product using id
router.put('/:id', auth.verify, auth.verifyAdmin, (request, response) => {
	ProductController.updateListing (request, response);
})

// Archive/Activate single product using id
router.put('/:id/status', auth.verify, auth.verifyAdmin, (request, response) => {
	ProductController.productStatusUpdate (request, response);
})

module.exports = router ;