const express = require('express');
const router = express.Router();
const DiscountController = require('../controllers/DiscountController.js');
const auth = require('../auth.js');

router.post('/add', auth.verify, auth.verifyAdmin, (request, response) => {
	DiscountController.addDiscount(request, response);
})

router.post('/update', auth.verify, auth.verifyAdmin, (request, response) => {
	DiscountController.updateDiscount (request, response);
})

router.post('/status', auth.verify, auth.verifyAdmin, (request, response) => {
	DiscountController.discountStatusUpdate (request, response);
})

router.post('/all', auth.verify, auth.verifyAdmin, (request, response) => {
	DiscountController.showAllDiscount(request, response);
})
router.post('/', auth.verify, (request, response) => {
	DiscountController.showDiscount(request, response);
})



module.exports = router;