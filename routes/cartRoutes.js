const express = require('express');
const router = express.Router();
const CartController = require('../controllers/CartController.js');
const auth = require('../auth.js');

// Add to cart
router.post('/add', auth.verify, (request, response) => {
	CartController.addtoCart(request, response);
})

router.post('/remove', auth.verify, (request, response) => {
	CartController.removeFromCart (request, response);
})

router.post('/', auth.verify, (request, response) => {
	OrderController.getCart (request, response);
})
module.exports = router ;