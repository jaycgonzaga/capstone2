const express = require('express');
const router = express.Router();
const ShippingController = require('../controllers/ShippingController.js');
const auth = require('../auth.js');

// Add shipping option
router.post('/add', auth.verify, auth.verifyAdmin, (request, response) => {
	ShippingController.addShipping(request, response);
})

// Update shipping option
router.post('/update', auth.verify, auth.verifyAdmin, (request, response) => {
	ShippingController.updateShipping (request, response);
})

// Activate/Deactivate shipping option status
router.post('/status', auth.verify, auth.verifyAdmin, (request, response) => {
	ShippingController.ShippingStatusUpdate (request, response);
})

// Show all existing shipping option
router.post('/all', auth.verify, auth.verifyAdmin, (request, response) => {
	ShippingController.showAllShipping(request, response);
})

// Show all existing and active shipping option
router.post('/', auth.verify, (request, response) => {
	ShippingController.showShipping(request, response);
})



module.exports = router;