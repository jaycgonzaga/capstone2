const express = require('express');
const router = express.Router();
const OrderController = require('../controllers/OrderController.js');
const auth = require('../auth.js');

router.post('/submit', auth.verify, (request, response) => {
	OrderController.submitOrder(request, response);
})

router.post('/cancel', auth.verify, (request, response) => {
	OrderController.cancelOrder (request, response);
})

router.post('/all', auth.verify, (request, response) => {
	OrderController.getAllOrder (request, response);
})

router.post('/', auth.verify, (request, response) => {
	OrderController.getAllActiveOrder (request, response);
})

module.exports = router;