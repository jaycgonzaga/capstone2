const express = require('express');
const router = express.Router();
const WishListController = require('../controllers/WishListController.js');
const auth = require('../auth.js');

router.post('/add', auth.verify, (request, response) => {
	WishListController.addToList(request, response);
})

router.post('/remove', auth.verify, (request, response) => {
	WishListController.removeFromList (request, response);
})

router.get('/', (request, response) =>{
	WishListController.getList(request, response);
})

module.exports = router;