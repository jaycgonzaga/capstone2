const express = require('express');
const router = express.Router();
const UserController = require('../controllers/UserController.js');
const auth = require('../auth.js');

// Check if user exists
router.post('/check-user', (request, response) => {
	UserController.checkUserExists(request.body).then((result) => {
		response.send(result);
	});
})

// Register user
router.post('/register', (request, response) => {
	UserController.registerUser(request.body).then((result) => {
		response.send(result);
	})
})

// Login user
router.post('/login', (request, response) => {
	UserController.loginUser(request, response);
})

// Search Profile
router.post("/searchprofile", (request, response) => {
	UserController.searchProfile(request.body).then((result) => {
		response.send(result);
	});
});


// View Own Profile
router.post("/profile", auth.verify, (request, response) => {
	UserController.getOwnProfile(request.body).then((result) => {
		response.send(result);
	});
});

// Update Own Profile
router.post("/profile/update", auth.verify, (request, response) => {
	UserController.updateProfile(request.body).then((result) => {
		response.send(result);
	});
});


// Search user Profile as an admin
router.post("/admin/profile", auth.verify, auth.verifyAdmin, (request, response) => {
	UserController.getProfileAdmin(request.body).then((result) => {
		response.send(result);
	});
});



module.exports = router;

