const mongoose = require('mongoose');

// Schema
const shipping_schema = new mongoose.Schema({
	
        description : {
            type: String,
            require: [true, "Shipping description required!"]
        },
        price : {
            type: Number,
            require: [true, "Shipping price required!"]
        },
        isActive : {
            type: Boolean,
            default: true
        }
        
})
module.exports = mongoose.model("Shipping", shipping_schema);