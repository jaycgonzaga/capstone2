const mongoose = require('mongoose');

// Schema
const discount_schema = new mongoose.Schema({
	
        description : {
            type: String,
            require : [true, "Discount description required!"]
        },
        discount : {
            type: Number,
            require : [true, "Discount value required!"]
        },
        isActive: {
            type: Boolean,
            defaul: true
        }
        
})
module.exports = mongoose.model("Discount", discount_schema);