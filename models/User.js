const mongoose = require('mongoose');

// Schema
const user_schema = new mongoose.Schema({
	firstName: {
		type: String,
		required: [true, 'First name is required!']
	},
	lastName: {
		type: String,
		required: [true, 'Last name is required!']
	},
	username: {
		type: String,
		required: [true, 'Username is required!']
	},
	email: {
		type: String,
		required: [true, 'E-mail is required!']
	},
    address: {
		type: String,
		required: [true, 'Address is required!']
	},
	password: {
		type: String,
		required: [true, 'Password is required!']
	},
	isAdmin: {
		type: Boolean,
		default: false
	},
	mobileNo: {
		type: String,
		required: [true, 'Mobile Number is required!']
	},
	wishlist: [
			{
			productId: {
				type: String,
				required: [true, 'Product is required!']
			},  
            price: {
				type: Number,
			}, 
		}
	]
})


module.exports = mongoose.model('User', user_schema);