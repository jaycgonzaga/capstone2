const mongoose = require('mongoose');

// Schema
const cart_schema = new mongoose.Schema({
	userId : {
        type : String,
        required : [true, "UserID is required"]
    },
    cartOwner: {
        type: String,
    },
    products : [{
        productId : {
            type: String,
            required : [true, "Product is required"]
        },
        name : {
            type: String,
        },
        price : {
            type : Number,
            required : [true, "Price is required"]
        },
        quantity : {
            type : Number,
            required : [true, "Quantity is required"]
        },
        subTotal : {
            type: Number,
            default : 0
        }
    }],
    Total : {
        type : Number,
        default: 0
    },
   
    createdOn : {
        type : Date,
        // The "new Date()" expression instantiates a new "date" that stores the current date and time whenever a course is created in our database
        default : new Date()
    },
})

module.exports = mongoose.model("Cart", cart_schema);