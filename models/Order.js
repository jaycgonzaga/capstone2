const mongoose = require('mongoose');

// Schema
const order_schema = new mongoose.Schema({
	userId : {
        type : String,
        required : [true, "UserID is required"]
    },
    fullName: {
        type : String,
        required: [true, "Name is required"]
    },
    address : {
        type : String,
        required : [true, "Address is required"]
    },
    cartId: {
        type: String,
        required : [true, "CartID is required"]
    },
    discountCode :[ {
        discountId : {
            type: String
        },
        discount : {
            type: Number
        },
        
    }],
    shippingOption :[ {
        duration : {
            type: String
        },
        price : {
            type: Number
        },
        
    }],
    Total : {
        type : Number,
        default: 0
    },
   
    orderedOn : {
        type : Date,
        default : new Date()
    },
    isOrderActive: {
        type : Boolean,
        default : true
    }
})

module.exports = mongoose.model("Order", order_schema);