const User = require('../models/User.js');
const bcrypt = require('bcrypt');
const auth = require('../auth.js');

// Check username/email
module.exports.checkUserExists = (request_body) => {
        return User.find({$or:[{email: request_body.email}, {username: request_body.username}]}).then((result, error) => {
            if(error) {
                return {
                    message: error.message
                }
            }
            if(result.length <= 0){
                return {
                    message: "Please enter username/email."
                }
            }
            if (result.length > 0) {
                return {
                    message: "Username/email does not exist. Register an account now!"
                }
            }
            
            // This will only return true if there are no errors AND there is an existing user from the database.
            return {
                message: "Username/email exists!"
            };
        })
}

// Registration
module.exports.registerUser = (request_body) => {
	let new_user = new User ({
		firstName: request_body.firstName,
		lastName: request_body.lastName,
		email: request_body.email,
        username: request_body.username,
        address: request_body.address,
		mobileNo: request_body.mobileNo,
		password: bcrypt.hashSync(request_body.password, 10)
	});

	return new_user.save().then((registered_user, error) => {
		if(error){
			return {
				message: error.message
			};
		}

		return {
			message: 'Successfully registered a user!'
		};
	}).catch(error => console.log(error));
}

// Login
module.exports.loginUser = (request, response) => {
	return User.findOne({$or:[{email: request.body.email}, {username: request.body.username}]}).then(result => {
		// Checks if a user is found with an existing email/username
		if(result == null){
			return response.send({
				message: "Username/email does not exist."
			})
		}

		// If a user was found with an existing email, then check if the password of that user matched the input from the request body
		const isPasswordCorrect = bcrypt.compareSync(request.body.password, result.password);

		if(isPasswordCorrect){
			// If the password comparison returns true, then respond with the newly generated JWT access token.
			return response.send({accessToken: auth.createAccessToken(result)});
		} else {
			return response.send({
				message: 'Your password is incorrect.'
			})
		}
	}).catch(error => response.send(error));
}
// Search user for non-admin users
module.exports.searchProfile = (request_body) => {
	return User.find({$or:[{_id: request_body.id}, {email: request_body.email}, {username: request_body.username}]}).select("-password -isAdmin -orders -mobileNo -address").then((result, error) => {
		if(error){
			return {
				message: error.message
			}
		}

		return result;
           
    
	});
}

// Get own profile 
module.exports.getOwnProfile = (request_body) => {
	return User.find({$or:[{_id: request_body.id}, {email: request_body.email}, {username: request_body.username}]}).select("-isAdmin").then((result, error) => {
		if(error){
			return {
				message: error.message
			}
		}

		return result;
           
    
	});
}

// Update profile
module.exports.updateProfile = (request, response) => {
	let update_user_details = {
		firstName: request_body.firstName,
		lastName: request_body.lastName,
		email: request_body.email,
        username: request_body.username,
        address: request_body.address,
		mobileNo: request_body.mobileNo,
		password: bcrypt.hashSync(request_body.password, 10)
	};

	return User.findByIdAndUpdate(request.params.id, updated_user_details).then((update, error) => {
		if (error){
			return response.send({
				message: error.message
			})
		}

		return response.send({
			message: ' Profile successfully updated!'
		})
	})
}


