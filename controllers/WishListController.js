const Product = require('../models/Product.js');
const User = require('../models/User.js');

//Add to wishlist
module.exports.addToList = async (request, response) => {
	// Blocks this function if the user is an admin
	if(request.user.isAdmin){
		return response.send('Action Forbidden');
	}
   
    if(request.user.productId == request.body.productId){
        return response.send('Product already in your wishlist!')
    }

    let getProduct = await Product.findById(request.body.productId).then(result => {
		return result;
	})
	// This variable will return true once the user data has been updated
	let isUserUpdated = await User.findById(request.user.id).then(user => {

		let add_list = {
			productId: request.body.productId,
            price: getProduct.price,
            quantity: request.body.quantity
		}

		user.wishlist.push(add_list);
		return user.save().then(updated_list => true).catch(error => error.message);
	})

	// Sends any error within 'isUserUpdated' as a response
	if(isUserUpdated !== true){
		return response.send({ message: isUserUpdated });
	}


	// [SECTION] Once isUserUpdated AND isCourseUpdated return true
	if(isUserUpdated  == true){
		return response.send({ message: 'Added to wishlist successfully!' });
	}
}

//Remove from cwishlist
module.exports.removeFromList = async (request, response) => {
   // Blocks this function if the user is an admin
	if(request.user.isAdmin){
		return response.send('Action Forbidden');
	}
   
  
	// This variable will return true once the user data has been updated
	let isUserUpdated = await User.findById(request.user.id).then(user => {

		let remove_list = {
			productId: request.body.productId,
            price: user.price,
            quantity: user.quantity,
		}
		user.wishlist.pull(remove_list);

		return user.save().then(updated_list => true).catch(error => error.message);
	})

	// Sends any error within 'isUserUpdated' as a response
	if(isUserUpdated !== true){
		return response.send({ message: isUserUpdated });
	}

	


	// [SECTION] Once isUserUpdated AND isCourseUpdated return true
	if(isUserUpdated  == true){
		return response.send({ message: 'Removed from wishlist successfully!' });
	}
}

//Get List
module.exports.getList= (request, response) => {
	return User.find({userId: request.user.id}).then(result => {
		return response.send(result.wishlist);
	})
}