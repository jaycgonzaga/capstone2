const Product = require('../models/Product.js');
const User = require('../models/User.js');

// Create Product Listing
module.exports.createListing = (request_body) => {
	let new_product = new Product ({
		name: request_body.name,
		description: request_body.description,
		price: request_body.price,
	});

	return new_product.save().then((created_product, error) => {
		if(error){
			return {
				message: error.message
			};
		}

		return {
			message: 'Successfully created a product listing!'
		};
	}).catch(error => console.log(error));
}

// Retrieve all Products
module.exports.getAllProduct= (request, response) => {
	return Product.find({}).then(result => {
		return response.send(result);
	})
}


// Retrieve all Active Products
module.exports.getAllActiveProduct = (request, response) => {
	return Product.find({isActive: true}).then(result => {
		return response.send(result);
	})
}

// Retrieve Single Product
module.exports.getProduct = (request, response) => {
	return Product.findById(request.params.id).then(result => {
		return response.send(result);
	})
}

// Update Product Listing
module.exports.updateListing = (request, response) => {
	let updated_product_details = {
		name: request.body.name,
		description: request.body.description,
		price: request.body.price
	}

	return Product.findByIdAndUpdate(request.params.id, updated_product_details).then((product, error) => {
		if (error){
			return response.send({
				message: error.message
			})
		}

		return response.send({
			message: ' Product listing has been updated successfully!'
		})
	})
}

// Product Listing Activate/Archive
module.exports.productStatusUpdate = (request, response) => {
	let archive_listing = {
		isActive: false
	};

    let activate_listing = {
		isActive: true
	};

    if(request.body.isActive == true){
	return Product.findByIdAndUpdate(request.params.id, activate_listing).then((result, error) => {
		if(error){
			return response.send({message: error.message})
			}

			return response.send({message: "Product listing is now active!"});
		}) 
    }

    if(request.body.isActive == false){
        return Product.findByIdAndUpdate(request.params.id, archive_listing).then((result, error) => {
            if(error){
                return response.send({message: error.message})
                }

                return response.send({message: "Product listing is now archived!"});
            }) 
        }
    else {
        return response.send({message: "Unknown command"});
    }
}

