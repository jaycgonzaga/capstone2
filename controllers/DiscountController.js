const Discount = require('../models/Discount.js');

// Add discount coupon
module.exports.addDiscount = (request_body) => {
	let new_discount = new Discount ({
		description: request_body.description,
		discount: request_body.discount

	});

	return new_discount.save().then((created_discount, error) => {
		if(error){
			return {
				message: error.message
			};
		}

		return {
			message: 'Successfully created a discount coupon!'
		};
	}).catch(error => console.log(error));
}

// Retrieve all discount coupon
module.exports.showAllDiscount= (request, response) => {
	return Discount.find({}).then(result => {
		return response.send(result);
	})
}


// Retrieve all active and existing discount coupon
module.exports.showDiscount = (request, response) => {
	return Discount.find({isActive: true}).then(result => {
		return response.send(result);
	})
}


// Update discount coupon
module.exports.updateDiscount = (request, response) => {
	let updated_product_details = {
		description: request.body.description,
		discount: request.body.discount,
	}

	return Product.findByIdAndUpdate(request.params.id, updated_discount_details).then((product, error) => {
		if (error){
			return response.send({
				message: error.message
			})
		}

		return response.send({
			message: ' Discount coupon has been updated successfully!'
		})
	})
}

// Activate/Deactivate discount coupon
module.exports.discountStatusUpdate = (request, response) => {
	let archive_discount = {
		isActive: false
	};

    let activate_discount = {
		isActive: true
	};

    if(request.body.isActive == true){
	return Discount.findByIdAndUpdate(request.params.id, activate_coupon).then((result, error) => {
		if(error){
			return response.send({message: error.message})
			}

			return response.send({message: "Discount coupon is now active!"});
		}) 
    }

    if(request.body.isActive == false){
        return Discount.findByIdAndUpdate(request.params.id, archive_coupon).then((result, error) => {
            if(error){
                return response.send({message: error.message})
                }

                return response.send({message: "Discount coupon is now archived!"});
            }) 
        }
    else {
        return response.send({message: "Unknown command"});
    }
}