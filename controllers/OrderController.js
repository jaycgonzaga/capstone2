const Product = require('../models/Product.js');
const UserCart = require('../models/UserCart.js');
const User = require('../models/User.js');
const Order = require('../models/Order.js');

// Submit order
module.exports.submitOrder = async (request, response) => {

	// If admin 
	if(request.user.isAdmin){
		return response.send('Action Forbidden');
	}

	// To get specific user
	let user = await User.findById(request.user.id);

    // To get discount code
    let discountCode = await Discount.findById(request.body.discountId);

    // To get shipping option
    let shippingOption = await Shipping.findById(request.body.shippingId);

	// To get user ordering fullname
	let fullname = `${user.firstName} ${user.lastName}`;

	// To get specific cart
    let cart = await UserCart.findOne({userId: request.user.id})
	
	//[Section] Sumbit order with discount
		if (cart && discountCode) {
			let add_order = new Order ({
                userId: user._id,
                fullName: fullname,
                address: user.address,
                cartId: cart._id,
                discountCode: [{
                    discountId: request.body.discountId,
                    description: discountCode.description
                }],
                shippingOption: [{
                    shippingId: request.body.shippingId,
                    description: shippingOption.description
                }],
				Total : (cart.Total + shippingOption.price) * discountCode.discount
			    });

                return add_order.save().then((updated_cart, error) => {
                    if (error){
                        return response.send({
                            message: error.message
                        })
                    }
            
                    return response.send({
                        message: 'Order submitted successfully!'
                    })
                    })
			  }

        if (cart){
            let add_order = new Order ({
                userId: user._id,
                fullName: fullname,
                address: user.address,
                cartId: cart._id,
                shippingOption: [{
                    shippingId: request.body.shippingId,
                    description: shippingOption.description
                }],
				Total : cart.Total + shippingOption.price
			});
            return add_order.save().then((updated_cart, error) => {
                if (error){
                    return response.send({
                        message: error.message
                    })
                }
        
                return response.send({
                    message: 'Order submitted successfully!'
                })
                })
            } else {
                return response.send({
                    message: 'Cart not found.'
                })
            }	
		  
} 
            
// Cancel order
module.exports.cancelOrder = async (request, response) => {

	if(request.user.isAdmin){
		return response.send('Action Forbidden');
	}
	let getOrder = await Order.findById(request.body.orderId);
    

	if(!getOrder || !getOrder.isActive){
		return response.send({ message: 'Order does not exist' });
	}

	if (getOrder) {
		let itemIndex = cart.products.findIndex(p => p.productId == request.body.productId);
		let cancel_order = {
			isOrderActive: false
		}
		  
		return Order.findByIdAndUpdate(request.body.orderId, cancel_order).then((result, error) => {
            if(error){
                return response.send({message: error.message})
                }
    
                return response.send({message: "Order cancelled succesfully."});
            }) 
		 
		
	}
}

// Retrieve all Order
module.exports.getAllOrder= (request, response) => {
	return Order.find({userId: request.user.id}).then(result => {
		return response.send(result);
	})
}


// Retrieve all Active Products
module.exports.getAllActiveOrder = (request, response) => {
	return Product.find({$and: [{userId: request.user.id}, {isActive: true}] }).then(result => {
		return response.send(result);
	})
}