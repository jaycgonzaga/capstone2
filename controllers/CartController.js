const Product = require('../models/Product.js');
const UserCart = require('../models/UserCart.js');
const User = require('../models/User.js');

// Add to cart feature
module.exports.addtoCart = async (request, response) => {
	const caltotal = (_cart) => {
		let total = 0;
		_cart.products.forEach(x => {
			total = total + x.subTotal
		})
		_cart.Total = total
	}

	// If admin 
	if(request.user.isAdmin){
		return response.send('Action Forbidden');
	}

	// To get specific user
	let user = await User.findById(request.user.id);

	// To get specific product
	let getProduct = await Product.findById(request.body.productId);
	
	
	
	// To get cart owner fullname
	let fullname = `${user.firstName} ${user.lastName}`;

	// To get specific cart
    let cart = await UserCart.findOne({userId: request.user.id})
	

	//[Section] Add to cart 
		if (cart) {
			let add_cart = {
				
				productId: request.body.productId,
				name : getProduct.name,
				price: getProduct.price,
				quantity: request.body.quantity,
				subTotal: (request.body.quantity * getProduct.price)
			}

			// To check if product exist
			let productExist = false;
			cart.products.forEach(p => {
				if(p.productId == request.body.productId){
					p.quantity += request.body.quantity;
					p.subTotal = (p.quantity * p.price);
					caltotal(cart)
					// cart.Total = cart.Total+p.subTotal;
					productExist = true;
				}
			}) 
			
			//if product does not exists in cart, add new item
			 if(!productExist) {
				// cart.Total = cart.Total + add_cart.subTotal
				
				cart.products.push(add_cart);
				caltotal(cart)
			  }
			  
			return cart.save().then((updated_cart, error) => {
				if (error){
					return response.send({
						message: error.message
					})
				}
		
				return response.send({
					message: 'Added to cart successfully!'
				})
			  })
			  
				} else {
					//no cart for user, create new cart
					let newCart = new UserCart({
					userId: request.user.id,
					cartOwner: fullname,
					products: [
						{ 	
							productId: request.body.productId,
							name : getProduct.name,
							price: getProduct.price,
							quantity: request.body.quantity, 
							subTotal: (request.body.quantity * getProduct.price)
						}
					],
					Total: (request.body.quantity * getProduct.price)
					});
					
					return newCart.save().then((updated_cart, error) => {
						if (error){
							return response.send({
								message: error.message
							})
						}
						return response.send({
							message: 'Added to cart successfully!'
						})
					})
				}

				
}
	
// Remove from cart feature
module.exports.removeFromCart = async (request, response) => {

	// To check if user is admin
	if(request.user.isAdmin){
		return response.send('Action Forbidden');
	}

	// Obtain information from database 
	let getProduct = await Product.findById(request.body.productId);
    let cart = await UserCart.findOne({userId: request.user.id});

	// To check if product exists or is not active
	if(!getProduct || !getProduct.isActive){
		return response.send({ message: 'Product does not exist or is not active' });
	}
	

	

	if (cart) {
		// To check if product exists in the cart
		cart.products.forEach(p => {
			if(p.productId == request.body.productId){
				let remove_cart = {
					productId: p.productId,
					name : p.name,
					price: p.price,
					quantity: p.quantity,
					subTotal: p.subTotal
				}

				// Automatic cart total function
				const caltotal = (_cart) => {
					let total = 0;
					_cart.products.forEach(x => {
						total = total + x.subTotal
					})
					_cart.Total = total
				}
				
				cart.products.pull(remove_cart);
				caltotal(cart)
				return cart.save().then((updated_cart, error) => {
					if(error){
						return {
							message: error.message
						};
					}
					return response.send({
						message: 'Successfully removed from cart'
					})
				})
			} else {
				return response.send({
					message : 'Product does not exist in your cart.'
				})
			}
		}) 
	}
}

// Get cart
module.exports.getCart= (request, response) => {
	return Cart.find({userId: request.user.id}).then(result => {
		return response.send(result);
	})
}

// Update cart feature
module.exports.removeFromCart = async (request, response) => {

	// To check if user is admin
	if(request.user.isAdmin){
		return response.send('Action Forbidden');
	}

	// Obtain information from database 
	let getProduct = await Product.findById(request.body.productId);
    let cart = await UserCart.findOne({userId: request.user.id});

	// To check if product exists or is not active
	if(!getProduct || !getProduct.isActive){
		return response.send({ message: 'Product does not exist or is not active' });
	}
	// Automatic cart total function
	const caltotal = (_cart) => {
		let total = 0;
		_cart.products.forEach(x => {
			total = total + x.subTotal
		})
		_cart.Total = total
	}

	// To check if product exists in the cart
		let productExist = false;
		cart.products.forEach(p => {
			if(p.productId == request.body.productId){
				p.quantity = request.body.quantity;
				p.subTotal = (p.quantity * p.price);
				caltotal(cart)
				productExist = true;
			}
		}) 

	if (cart) {
		let itemIndex = cart.products.findIndex(p => p.productId == request.body.productId);
		let remove_cart = {

			productId: request.body.productId,
			name : getProduct.name,
            price: getProduct.price,
            quantity: request.body.quantity
		}
		if (itemIndex > -1) {
			cart.products.pull(remove_cart);
		  } else{
			return response.send({
				message : 'Product does not exist in your cart.'
			});
		  }
		  
		return cart.save().then((updated_cart, error) => {
			if(error){
				return {
					message: error.message
				};
			}
			return response.send({
				message: 'Successfully removed from cart'
			})
		})
		 
		} else {
		return response.send ({
			message: "Cart not found or does not exists."
		})
	}
}

