const Shipping = require('../models/Shipping.js');

// Add shipping option 
module.exports.addShipping = (request_body) => {
	let new_shipping = new Shipping ({
		description: request_body.description,
		price: request_body.price

	});

	return new_shipping.save().then((created_shipping, error) => {
		if(error){
			return {
				message: error.message
			};
		}

		return {
			message: 'Successfully created a shipping coupon!'
		};
	}).catch(error => console.log(error));
}

// Retrieve all existing shipping options
module.exports.showAllOptions= (request, response) => {
	return Shipping.find({}).then(result => {
		return response.send(result);
	})
}


// Retrieve all existing and active shipping options
module.exports.showOptions = (request, response) => {
	return Shipping.find({isActive: true}).then(result => {
		return response.send(result);
	})
}


// Update shipping option using id
module.exports.updateShipping = (request, response) => {
	let updated_product_details = {
		description: request.body.description,
		price: request.body.price,
	}

	return Shipping.findByIdAndUpdate(request.params.id, updated_shipping_details).then((product, error) => {
		if (error){
			return response.send({
				message: error.message
			})
		}

		return response.send({
			message: ' Shipping option has been updated successfully!'
		})
	})
}

// Product Listing Activate/Archive
module.exports.shippingStatusUpdate = (request, response) => {
	let archive_shipping = {
		isActive: false
	};

    let activate_shipping = {
		isActive: true
	};

    if(request.body.isActive == true){
	return Shipping.findByIdAndUpdate(request.params.id, activate_option).then((result, error) => {
		if(error){
			return response.send({message: error.message})
			}

			return response.send({message: "Shipping option is now active!"});
		}) 
    }

    if(request.body.isActive == false){
        return Shipping.findByIdAndUpdate(request.params.id, archive_option).then((result, error) => {
            if(error){
                return response.send({message: error.message})
                }

                return response.send({message: "Shipping option is now archived!"});
            }) 
        }
    else {
        return response.send({message: "Unknown command"});
    }
}