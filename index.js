const express = require('express');
const app = express();
const mongoose = require('mongoose');
const cors = require('cors');
require('dotenv').config();
const port = 4000;
const userRoutes = require('./routes/userRoutes.js');
const productRoutes = require('./routes/productRoutes.js');
const cartRoutes = require('./routes/cartRoutes.js');
const wishListRoutes = require('./routes/wishListRoutes.js');
const orderRoutes = require('./routes/orderRoutes.js');
const shippingRoutes = require('./routes/shippingRoutes.js');
const discountRoutes = require('./routes/discountRoutes.js');

// Middleware
app.use(express.json());
app.use(express.urlencoded({extended: true}));
app.use(cors());

// Routes
app.use('/api/cart', cartRoutes);
app.use('/api/users', userRoutes);
app.use('/api/products', productRoutes);
app.use('/api/wishlist', wishListRoutes);
app.use('/api/order', orderRoutes);
app.use('/api/shipping', shippingRoutes);
app.use('/api/discounts', discountRoutes);


// Database
mongoose.connect(`mongodb+srv://admin:${process.env.MONGODB_PASSWORD}@b303-gonzaga.0c0ikhd.mongodb.net/b303-capstone2-ecommerce-api?retryWrites=true&w=majority`, {
	useNewUrlParser: true,
	useUnifiedTopology: true
});

mongoose.connection.on('error', () => console.log("Can't connect to database."));
mongoose.connection.once('open', () => console.log("Connected to MongoDB!"));

app.listen(process.env.PORT || port, () => {
	console.log(`Ecommerce System API is now running at localhost:${process.env.PORT || port}`);
})

module.exports = app;